
..
    THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_project V0.3.27

maios django 0.3.0 documentation
################################

welcome to the documentation of the maios django 0.3.0 project.


.. include:: features_and_examples.rst


code maintenance guidelines
***************************


portions code requirements
==========================

    * pure python
    * fully typed (:pep:`526`)
    * fully :ref:`documented <-portions>`
    * 100 % test coverage
    * multi thread save
    * code checks (using pylint and flake8)


design pattern and software principles
======================================

    * `DRY <http://en.wikipedia.org/wiki/Don%27t_repeat_yourself>`_
    * `KISS <http://en.wikipedia.org/wiki/Keep_it_simple_stupid>`_


.. include:: ../CONTRIBUTING.rst


main module
***********

.. autosummary::
    :toctree: _autosummary
    :nosignatures:

    maios



indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* `portion repositories at gitlab.com <https://gitlab.com/ae-group>`_
