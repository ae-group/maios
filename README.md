# Kairos La Gomera Website Project

this is a django CMS website project made for the
[Non-profit Money-less, Time, Goods and Rental Exchange Association in La Gomera / Canary Islands](
https://kairos-gomera.org/).

this website got initially created with WordPress/Elementor. later it got migrated with this project to Django CMS
using [JetBrains PyCharm](https://www.jetbrains.com/pycharm/), the fantastic and IMO world's best Python IDE.


## documentation

the documentation [at Read The Docs](https://kairos.readthedocs.io/en/latest/) is bundling RST document files of this
project, including the manuals for:

* [members](https://gitlab.com/ae-group/kairos/-/blob/develop/docs/member_manual.rst)
* [administrators](https://gitlab.com/ae-group/kairos/-/blob/develop/docs/administrator_manual.rst)
* [programmers](https://gitlab.com/ae-group/kairos/-/blob/develop/docs/programmer_manual.rst)

this includes also [a RST about the features of this website](
https://gitlab.com/ae-group/kairos/-/blob/develop/docs/features_and_examples.rst)

[how to contribute to this project is explained here](
https://gitlab.com/ae-group/kairos/-/blob/develop/CONTRIBUTING.rst)
